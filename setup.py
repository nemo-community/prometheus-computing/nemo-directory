from setuptools import setup, find_packages

setup(
	name='nemo_directory',
	version='1.0.0',
	python_requires='>=3.7',
	packages=find_packages(),
	include_package_data=True,
	url='https://gitlab.com/nemo-community/atlantis-labs/nemo-directory',
	author='Atlantis Labs LLC',
	author_email='atlantis@atlantislabs.io',
	description='Small directory for multiple NEMO application, with URLs and tools available',
	long_description='Small directory for multiple NEMO application, with URLs and tools available',
	license="MIT",
	classifiers=[
		'Development Status :: 5 - Production/Stable',
		'Intended Audience :: Science/Research',
		'Natural Language :: English',
		'Operating System :: OS Independent',
		'Programming Language :: Python :: 3.7',
	],
	install_requires=[
		'Flask==3.0.3',
		'gunicorn==22.0.0',
	],
)
