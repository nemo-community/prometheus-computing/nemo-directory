import json
import ssl
import sys
import urllib.request
from datetime import datetime
from http.client import HTTPResponse
from typing import List
from urllib.error import URLError

from flask import Flask, render_template, url_for

application = Flask(__name__)
application.config.from_envvar("APPLICATION_SETTINGS", silent=True)


class NEMOApp(object):
    def __init__(self, name, url, registration_url=None):
        self.name = name
        self.url = url
        self.registration_url = registration_url
        self.tools = []
        self.areas = []
        self.error = None
        self.tool_control_url = url + "/tool_control/"
        self.show_tool_owner = True
        self.email_tool_owner = True


# Global variable
nemo_app_list: List = []
last_update: datetime = None


@application.route("/")
def hello_world():
    global nemo_app_list
    if should_refresh_app_list():
        refresh_app_list()
    page_title = application.config.get("PAGE_TITLE", "NEMO Directory")
    show_non_visible_tools = application.config.get("SHOW_NON_VISIBLE_TOOLS", False)
    return render_template(
        "index.html",
        title=page_title,
        show_non_visible_tools=show_non_visible_tools,
        nemo_apps=nemo_app_list,
    )


def refresh_app_list():
    global nemo_app_list, last_update
    print(f"[{datetime.now()}] refreshing app list", file=sys.stdout)
    for nemo_app_config in application.config.get("NEMO_APPLICATIONS", []):
        base_url = nemo_app_config["url"]
        base_api_url = base_url + "/api"
        nemo_app = NEMOApp(nemo_app_config["name"], base_url, nemo_app_config.get("registration_url"))
        nemo_app.show_tool_owner = nemo_app_config.get("show_tool_owner", True)
        nemo_app.email_tool_owner = nemo_app_config.get("email_tool_owner", True)
        try:
            timeout = nemo_app_config.get("timeout", 3)
            ca_file = nemo_app_config.get("certificate_path")
            headers = nemo_app_config.get("headers", {})
            if nemo_app_config.get("tools", True):
                tools_api_url = base_api_url + "/tools?expand=_primary_owner"
                nemo_app.tools = api_request(tools_api_url, headers, timeout, ca_file)
            if nemo_app_config.get("areas", True):
                areas_api_url = base_api_url + "/areas"
                nemo_app.areas = api_request(areas_api_url, headers, timeout, ca_file)
        except URLError as error:
            nemo_app.error = error.reason
            print(f"[{datetime.now()}] {nemo_app.error}", file=sys.stderr)
        except Exception as error:
            nemo_app.error = str(error)
            print(f"[{datetime.now()}] {nemo_app.error}", file=sys.stderr)
        if not nemo_app.error or application.config.get("DISPLAY_APP_WITH_ERRORS", True):
            nemo_app_list.append(nemo_app)
    print(f"[{datetime.now()}] app list updated", file=sys.stdout)
    last_update = datetime.now()


def api_request(url: str, headers: dict, timeout: int, ca_file: str = None):
    # TODO: handle 403 or 401 when permissions are not granted
    req = urllib.request.Request(url, headers=headers or {})
    context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=ca_file)
    with urllib.request.urlopen(req, timeout=timeout, context=context) as api_response:
        return json_object_response(api_response, headers, timeout, ca_file)


def json_object_response(response: HTTPResponse, headers, timeout: int, ca_file) -> List:
    read_response = response.read()
    if read_response:
        response_json = json.loads(read_response)
        if "results" and "next" in response_json:
            # paginated response
            json_objects: List = response_json.get("results", [])
            next_url = response_json.get("next")
            if next_url:
                json_objects.extend(api_request(next_url, headers, timeout, ca_file))
            return json_objects
        else:
            return response_json
    else:
        return {}


def should_refresh_app_list() -> bool:
    refresh_frequency = application.config.get("REFRESH_FREQUENCY", 60)
    global nemo_app_list, last_update
    now = datetime.now()
    return nemo_app_list == [] or not last_update or ((last_update - now).total_seconds() / 60) > refresh_frequency
