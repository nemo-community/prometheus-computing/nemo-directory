FROM python:3.11
ARG install_dir="/tmp/nemo_dir/"
ENV APPLICATION_SETTINGS="/opt/nemo_directory/config.py"

COPY setup.py MANIFEST.in $install_dir
COPY nemo_directory $install_dir/nemo_directory
RUN pip install $install_dir
RUN rm --recursive --force $install_dir
CMD ["gunicorn","--capture-output", "--bind=0.0.0.0:8000", "nemo_directory.app:application"]
EXPOSE 8000/tcp