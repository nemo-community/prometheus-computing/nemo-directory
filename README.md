# NEMO Directory

Small web application to collect data from different versions of NEMO (tools, etc.)

## Prerequisite
This app uses token authentication to communicate with NEMO's REST API.

To enable token authentication in NEMO's `settings.py`:
* add `'rest_framework.authtoken',` to `INSTALLED_APPS`
* add `'rest_framework.authentication.TokenAuthentication',` to `DEFAULT_AUTHENTICATION_CLASSES` in `REST_FRAMEWORK`

Then use the detailed administration in NEMO to create a user with permissions (at a minimum view tools and view areas) and use the detailed administration `token` page to generate the token associated with the user.

## Quick start
```shell
docker run --detach --publish "8000:8000" --volume <path_to_config.py>:/opt/nemo_directory/config.py registry.gitlab.com/nemo-community/atlantis-labs/nemo-directory
```

## Configuration

mount a `config.py` to `/opt/nemo_directory/config.py`

or set the environment variable `APPLICATION_SETTINGS` to the path of your `config.py`


the config.py file should look like the following (commented out are optional default values):

```python
# PAGE_TITLE = "NEMO Directory"
# DISPLAY_APP_WITH_ERRORS = True
# SHOW_NON_VISIBLE_TOOLS = False
# REFRESH_FREQUENCY = 60

NEMO_APPLICATIONS = [
    {
        "name": "NEMO 1",
        "url": "https://nemo_one.com",
        "registration_url": "https://company.com/nemo_registration",
        "headers": {"Authorization": "Token <token_value>"},
        # "certificate_path": None,
        # "timeout": 1,
        # "areas": True,
        # "tools": True,
        # "email_tool_owner": True,
        # "show_tool_owner": True,
    }
]
```